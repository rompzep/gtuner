package com.example.tuner;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import be.tarsos.dsp.pitch.PitchProcessor;

public class TunerFragment extends Fragment {

    private ImageView pointer;
    private Button playEButton;
    private Button playAButton;
    private Button playDButton;
    private Button playGButton;
    private Button playBButton;
    private Button playESmallButton;
    private TextView tuningTextView;
    private TextView textNoteDecalage;

    private static List<Button> listButton;
    private static Accord accordCourant = null;
    private int width;
    private int cordeCourante = 0;
    private double seuil = 5.0;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Bundle bundle = intent.getExtras();
        Accord newAccord = (Accord) bundle.getSerializable("Accord");
        majAccord(newAccord);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tuner, container, false);

        pointer = (ImageView) rootView.findViewById(R.id.pointer);
        playEButton = (Button) rootView.findViewById(R.id.playEButton);
        playAButton = (Button) rootView.findViewById(R.id.playAButton);
        playDButton = (Button) rootView.findViewById(R.id.playDButton);
        playGButton = (Button) rootView.findViewById(R.id.playGButton);
        playBButton = (Button) rootView.findViewById(R.id.playBButton);
        playESmallButton = (Button) rootView.findViewById(R.id.playESmallButton);
        tuningTextView = (TextView) rootView.findViewById(R.id.tuningTextView);
        textNoteDecalage = (TextView) rootView.findViewById(R.id.textViewNoteDecalage);

        listButton = new ArrayList<Button>();
        listButton.addAll(Arrays.asList(playEButton, playAButton, playDButton, playGButton, playBButton, playESmallButton));
        accordCourant = (Accord) MusicUtility.loadObject(getActivity(), "last_accord");
        if (accordCourant == null)
            accordCourant = MusicUtility.getDefaultAccord(getActivity());
        setCorde(cordeCourante, "selected");
        majAccord(accordCourant);

        // initialise les listener des boutons des cordes et du textview de l'accord
        for (Button btn : listButton) {
            btn.setOnClickListener(PlayNoteListener);
            tuningTextView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), TuningChoiceActivity.class);
                    startActivityForResult(intent, 12345);
                }
            });
        }

        // ajuste l'axe des abcisses du tuner en fonction de la taille de l'écran
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;

        AudioDispatcher dispatcher =
                AudioDispatcherFactory.fromDefaultMicrophone(22050, 1024, 0);

        PitchDetectionHandler pdh = new PitchDetectionHandler() {
            @Override
            public void handlePitch(PitchDetectionResult res, AudioEvent e) {
                final float pitchInHz = res.getPitch();
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            double decalage = Math.round(pitchInHz - accordCourant.getNotes().get(cordeCourante).getFrequence());
                            if (Math.abs(decalage) < seuil)
                                setCorde(cordeCourante, "tuned");
                            int pos = (width/2 - 75);
                            if (pitchInHz != -1) {
                                pos = pos + (int) decalage;
                                if (decalage > 0)
                                    textNoteDecalage.setText("+" + decalage + " Hz");
                                else
                                    textNoteDecalage.setText("" + decalage + " Hz");
                            } else {
                                textNoteDecalage.setText("");
                            }
                            pointer.setX(pos);
                        }
                    });
                }
            }
        };

        AudioProcessor pitchProcessor = new PitchProcessor(PitchProcessor.PitchEstimationAlgorithm.FFT_YIN, 22050, 1024, pdh);
        dispatcher.addAudioProcessor(pitchProcessor);
        new Thread(dispatcher, "Audio Thread").start();

        return rootView;
    }

    private void majAccord(Accord a) {
        if (a != null) {
            accordCourant = a;
            MusicUtility.saveObject(getContext(), accordCourant, "last_accord");
            tuningTextView.setText("" + accordCourant.getNom());
            int numCorde = 0;
            for (Button corde : listButton) {
                setCorde(numCorde, "inactive");
                corde.setText(accordCourant.getNotes().get(numCorde).getNote());
                numCorde++;
            }
            setCorde(cordeCourante, "selected");
        }
    }

    private void setCordeFocus(int numCorde, Boolean focus) {
        if (focus)
            listButton.get(numCorde).setTextSize(TypedValue.COMPLEX_UNIT_DIP, 26);
        else
            listButton.get(numCorde).setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
    }

    private void setCorde(int numCorde, String statut) {
        if (statut == "selected") {
            listButton.get(numCorde).setBackgroundResource(R.drawable.selected);
            cordeCourante = numCorde;
            setCordeFocus(numCorde, Boolean.TRUE);
        } else if (statut == "inactive"){
            listButton.get(numCorde).setBackgroundResource(R.drawable.inactive);
            setCordeFocus(numCorde, Boolean.FALSE);
        } else if (statut == "tuned") {
            listButton.get(numCorde).setBackgroundResource(R.drawable.ok);
        }
    }

    private View.OnClickListener PlayNoteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (int i = 0; i < listButton.size(); i++) {
                if (listButton.get(i) == (Button) v.findViewById(v.getId())) {
                    setCorde(i, "selected");
                } else {
                    setCordeFocus(i, Boolean.FALSE);
                    if ((listButton.get(i).getBackground().getConstantState() != getResources().getDrawable(R.drawable.ok).getConstantState()))
                        setCorde(i, "inactive");
                }
            }
        }
    };

}
