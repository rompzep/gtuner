package com.example.tuner;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TuningChoiceActivity extends AppCompatActivity {

    private List<Accord> accords = null;
    private AccordListAdapter accordListAdapter = null;
    private ListView listView = null;

    public void selectAccord(Object a) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("Accord", (Serializable)a);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Bundle bundle = intent.getExtras();
        Accord newAccord = (Accord) bundle.getSerializable("Accord");
        if (newAccord != null) {
            MusicUtility.addCustomAccord(getApplicationContext(), newAccord);
            accords.add(newAccord);
            accordListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        selectAccord(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_accord_choix);

        accords = getListData();
        listView = (ListView) findViewById(R.id.listView);
        accordListAdapter = new AccordListAdapter(getApplicationContext(), accords);
        listView.setAdapter(accordListAdapter);

        // When the user clicks on the ListItem
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = listView.getItemAtPosition(position);
                Accord accord = (Accord) o;
                selectAccord(o);
            }
        });

        final Button addAccordBtn = (Button) findViewById(R.id.addAccord);
        addAccordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateTuningActivity.class);
                startActivityForResult(intent, 23456);
            }
        });
    }

    private List<Accord> getListData() {
        List<Accord> defaultTunings = MusicUtility.getAccords(getApplicationContext());
        List<Accord> customTunings = MusicUtility.getCustomAccords(getApplicationContext());
        defaultTunings.addAll(customTunings);
        return defaultTunings;
    }

}
