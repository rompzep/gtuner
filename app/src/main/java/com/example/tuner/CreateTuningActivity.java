package com.example.tuner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.icu.text.Transliterator;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static java.security.AccessController.getContext;

public class CreateTuningActivity extends AppCompatActivity {

    private EditText tuningTextView;
    private Button saveBtn;
    private static ArrayList<Button> cordesBtn;

    private ArrayList<Note> notesPossibles;
    private onClickInterface onClickNote;
    private int cordeCourante;

    @Override
    public void onBackPressed() {
        returnAccord(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // masque la barre de titre et de notifications
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_create_tuning);

        notesPossibles = (ArrayList<Note>) MusicUtility.getNotes(getApplicationContext());
        cordeCourante = 0;
        tuningTextView = (EditText) findViewById(R.id.custom_tuning);
        saveBtn = (Button) findViewById(R.id.saveAccord);

        cordesBtn = new ArrayList<Button>(Arrays.asList(
          (Button) findViewById(R.id.playEButton),
          (Button) findViewById(R.id.playAButton),
          (Button) findViewById(R.id.playDButton),
          (Button) findViewById(R.id.playGButton),
          (Button) findViewById(R.id.playBButton),
          (Button) findViewById(R.id.playESmallButton)));

        setCorde(cordeCourante, "selected");

        // listeners
        for (Button cordeBtn : cordesBtn)
            cordeBtn.setOnClickListener(SelectCordeListener);
        saveBtn.setOnClickListener(SaveAccordListener);
        onClickNote = new onClickInterface() {
            @Override
            public void setClick(int corde) {
                cordesBtn.get(cordeCourante).setText(notesPossibles.get(corde).getNote());
            }
        };

        // charge la liste des notes possibles
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        final RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, notesPossibles, onClickNote);
        recyclerView.setAdapter(adapter);
    }

    // focus/unfocus une corde
    private void setCordeFocus(int corde, Boolean focus) {
        if (focus)
            cordesBtn.get(corde).setTextSize(TypedValue.COMPLEX_UNIT_DIP, 26);
        else
            cordesBtn.get(corde).setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
    }

    // sélectionne/déselectionne une corde
    private void setCorde(int corde, String statut) {
        if (statut == "selected") {
            cordesBtn.get(corde).setBackgroundResource(R.drawable.selected);
            cordeCourante = corde;
            setCordeFocus(corde, Boolean.TRUE);
        } else if (statut == "inactive"){
            cordesBtn.get(corde).setBackgroundResource(R.drawable.inactive);
            setCordeFocus(corde, Boolean.FALSE);
        }
    }

    // gère le clic d'une corde
    private View.OnClickListener SelectCordeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            for (int i = 0; i < cordesBtn.size(); i++) {
                if (cordesBtn.get(i) == (Button) v.findViewById(v.getId())) {
                    setCorde(i, "selected");
                } else {
                    setCordeFocus(i, Boolean.FALSE);
                    setCorde(i, "inactive");
                }
            }
        }
    };

    // vérifie la validité de l'accord en cours d'édition
    private String checkAccord() {
        Toast.makeText(getApplicationContext(), "["+tuningTextView.getText().toString()+"]", Toast.LENGTH_SHORT).show();
        if (tuningTextView.getText().toString().isEmpty())
            return "Vous devez nommer votre accord !";
        for (Button corde : cordesBtn)
            if (corde.getText().toString().isEmpty())
                return "Votre accord est incomplet !";
        return "";
    }

    // retourne l'accord s'il est valide
    private View.OnClickListener SaveAccordListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String checkMsg = checkAccord();
            if (!checkMsg.isEmpty()) {
                Toast.makeText(getApplicationContext(), checkMsg, Toast.LENGTH_SHORT).show();
            } else {
                String nomAccord = tuningTextView.getText().toString();
                List<Note> notesAccord = new ArrayList<Note>();
                for (Button cordeBtn : cordesBtn) {
                    String noteStr = cordeBtn.getText().toString();
                    notesAccord.add(new Note(noteStr, MusicUtility.noteToFreq(getApplicationContext(), noteStr)));
                }
                Accord customAccord = new Accord(nomAccord, notesAccord);
                customAccord.setCustom();
                returnAccord(customAccord);
            }
        }
    };

    // retourne l'accord sous forme serialisée à l'activité appelante (celle du choix des accords)
    private void returnAccord(Accord a) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("Accord", (Serializable)a);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

}
