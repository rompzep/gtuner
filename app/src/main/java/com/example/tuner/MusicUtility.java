package com.example.tuner;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MusicUtility {

    // Par soucis de performances, on passe par les fichiers associés
    // aux ressources uniquement en cas de modifications.
    // Sinon, on retourne une copie de la variable associée à la ressource.
    // (copie pour empêcher toute modification à l'extérieur de cette classe utilitaire)
    static List<Note> notes = null;
    static List<Accord> accordsClassiques = null;
    static List<Accord> accordsPersonnalises = null;

    // retourne toutes les notes possibles depuis une ressource CSV
    public static List<Note> getNotes(Context context) {
        if (notes == null) {
            notes = new ArrayList<Note>();
            InputStream inputStream = context.getResources().openRawResource(R.raw.notes);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            try {
                String line = reader.readLine();
                String[] titles = line.split(",");
                int octave = 0;
                line = reader.readLine();
                while (line != null) {
                    String[] _frequences = line.split(",");
                    for (int i = 0; i < _frequences.length; i++) {
                        Note n = new Note(titles[i] + Integer.toString(octave), Double.valueOf(_frequences[i]));
                        notes.add(n);
                        Log.d("mytag", "note : " + n.getNote());
                    }
                    octave += 1;
                    line = reader.readLine();
                }
            } catch (Exception e) {}
        }
        return new ArrayList<>(notes);
    }

    // retourne la fréquence correspondant à la note
    public static double noteToFreq(Context context, String note) {
        List<Note> notes = getNotes(context);
        HashMap<String, Double> frequences = new HashMap<String, Double>();
        for (Note n : notes)
            frequences.put(n.getNote(), n.getFrequence());
        return frequences.get(note);
    }

    // retourne tous les accords par défaut depuis une ressource CSV
    public static List<Accord> getAccords(Context context) {
        if (accordsClassiques == null) {
            accordsClassiques = new ArrayList<Accord>();
            InputStream inputStream = context.getResources().openRawResource(R.raw.accords);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            try {
                String line = reader.readLine();
                while (line != null) {
                    List<Note> _notes = new ArrayList<Note>();
                    String[] data = line.split(",");
                    for (int i = 1; i < data.length; i++)
                        _notes.add(new Note(data[i], noteToFreq(context, data[i])));
                    accordsClassiques.add(new Accord(data[0], _notes));
                    line = reader.readLine();
                }
            } catch (Exception e) {}
        }
        return new ArrayList<>(accordsClassiques);
    }

    // retourne le premier de la liste des accords par défaut
    public static Accord getDefaultAccord(Context context) {
        return getAccords(context).get(0);
    }

    // ajoute un accord personnalisé
    // (dans la variable de classe et dans le fichier associé)
    public static void addCustomAccord(Context context, Accord accord) {
        List<Accord> update = getCustomAccords(context);
        update.add(accord);
        saveObject(context, update, "custom_tunings");
        accordsPersonnalises = update;
    }

    // supprime un accord personnalisé
    // (de la variable de classe et du fichier associé)
    public static void removeCustomAccord(Context context, int accord) {
        List<Accord> update = getCustomAccords(context);
        update.remove(accord);
        saveObject(context, update, "custom_tunings");
        accordsPersonnalises = update;
    }

    // retourne une copie de la liste des accords personnalisés
    // (vide si aucun accord)
    public static List<Accord> getCustomAccords(Context context) {
        if (accordsPersonnalises == null) {
            accordsPersonnalises = (List<Accord>) loadObject(context, "custom_tunings");
            if (accordsPersonnalises == null)
                return new ArrayList<Accord>();
        }
        return new ArrayList<>(accordsPersonnalises);
    }

    // enregistre un objet dans un fichier
    public static void saveObject(Context context, Object object, String filename) {
        try {
            FileOutputStream fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(object);
            os.close();
            fos.close();
        } catch (Exception e) {}
    }

    // retourne un objet enregistré dans un fichier
    // (null en cas d'échec)
    public static Object loadObject(Context context, String filename) {
        Object object = null;
        try {
            FileInputStream fis = context.openFileInput(filename);
            ObjectInputStream is = new ObjectInputStream(fis);
            object = (Object) is.readObject();
            is.close();
            fis.close();
        } catch (Exception e) {}
        finally {
            return object;
        }
    }

}