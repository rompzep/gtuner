package com.example.tuner;

import java.io.Serializable;
import java.util.List;

public class Accord implements Serializable {

    enum type {classic, custom}
    private String nom;
    private List<Note> notes;
    private type typeAccord;

    public Accord(String nom, List<Note> notes) {
        this.nom = nom;
        this.notes = notes;
        this.typeAccord = type.classic;
    }

    public Boolean isCustom() {
        return typeAccord == type.custom;
    }

    public Boolean isClassic() {
        return typeAccord == type.classic;
    }

    public void setCustom() {this.typeAccord = type.custom;}

    public void setClassic() {this.typeAccord = type.classic;}

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public String getNotesString() {
        String _notes = "";
        for (Note note : notes) {
            _notes += note.getNote() + ", ";
        }
        _notes = _notes.substring(0, _notes.length()-2);
        return _notes;
    }

}
