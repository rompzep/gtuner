package com.example.tuner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class AccordListAdapter extends BaseAdapter {
    private List<Accord> listAccord;
    private LayoutInflater layoutInflater;
    private Context context;

    public AccordListAdapter(Context context, List<Accord> listAccord) {
        this.listAccord = listAccord;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listAccord.size();
    }

    @Override
    public Object getItem(int position) {
        return listAccord.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // initialise les item de la listview
    // on activera un bouton remove en face des accords personnalisés
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.accord_list_item, null);
            holder = new ViewHolder();
            holder.removeBtn = (Button) convertView.findViewById(R.id.removeAccord);
            holder.accordView = (TextView) convertView.findViewById(R.id.textView_accord);
            holder.notesView = (TextView) convertView.findViewById(R.id.textView_notes);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Accord accord = this.listAccord.get(position);
        final int p = position;

        int visibility = View.INVISIBLE;
        Boolean clickable = false;
        if (accord.isCustom()) {
            visibility = View.VISIBLE;
            clickable = true;
        }
        holder.removeBtn.setVisibility(visibility);
        holder.removeBtn.setClickable(clickable);
        holder.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MusicUtility.removeCustomAccord(context, p - MusicUtility.getAccords(context).size());
                listAccord.remove(accord);
                notifyDataSetChanged();
            }
        });
        holder.accordView.setText(accord.getNom());
        holder.notesView.setText(accord.getNotesString());

        return convertView;
    }

    static class ViewHolder {
        Button removeBtn;
        TextView accordView;
        TextView notesView;
    }

}
