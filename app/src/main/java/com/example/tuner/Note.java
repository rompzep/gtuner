package com.example.tuner;

import java.io.Serializable;
import java.util.List;

public class Note implements Serializable {

    private String note;
    private Double frequence;

    public Note(String note, Double frequence) {
        this.note = note;
        this.frequence = frequence;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setFrequence(Double frequence) {
        this.frequence= frequence;
    }

    public Double getFrequence() {
        return frequence;
    }

}
