package com.example.tuner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;


import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.Timer;
import java.util.TimerTask;

public class MetronomFragment extends Fragment {

    private Button bstart;
    private Button bralentir;
    private Button baccelerer;

    private SeekBar seekBar;
    private EditText tempo;

    private static int rythme;

    boolean loaded = false;
    private static int soundId;
    private static SoundPool soundPool;

    private boolean play=false;
    private boolean jouait;

    private static Timer timer;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_metronom, container, false);

        bstart = (Button) root.findViewById(R.id.calcul);
        bralentir = (Button) root.findViewById(R.id.ralentir);
        baccelerer = (Button) root.findViewById(R.id.accelerer);

        bstart.setOnClickListener(Metronome);
        bralentir.setOnClickListener(Metronome);
        baccelerer.setOnClickListener(Metronome);


        tempo = (EditText) root.findViewById(R.id.tempo);


        soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {

            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) { loaded = true;}
        });
        soundId = soundPool.load(getActivity(), R.raw.beat, 1);



        seekBar = (SeekBar)root.findViewById(R.id.slider);
        seekBar.incrementProgressBy(1);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                rythme = progress;
                tempo.setText(String.valueOf(rythme));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                jouait = play;
                if(play){
                    stop();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(jouait && rythme>0){
                    start();
                }
            }
        });

        return root;
    }

    private View.OnClickListener Metronome = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            rythme =  Integer.parseInt(tempo.getHint().toString());
            String temp= tempo.getText().toString();
            if (!"".equals(temp)){
                rythme=Integer.parseInt(temp);
            }
            switch(v.getId()) {
                case R.id.calcul:
                    if(!play && rythme>0) { start();}else{stop();}
                    break;

                case R.id.accelerer:
                    if(rythme<240) {
                        rythme += 1;
                        tempo.setText(String.valueOf(rythme));
                        seekBar.setProgress(rythme);
                        if (play){restart();}
                    }
                    break;
                case R.id.ralentir:
                    if(rythme>0) {
                        rythme -= 1;
                        tempo.setText(String.valueOf(rythme));
                        seekBar.setProgress(rythme);
                        if (play){
                            restart();
                        }
                    }
                    break;
                default:
            }
        }
    };

    private void restart(){
        stop();start();
    }

    private void start(){
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                soundPool.play(soundId, 1, 1, 0, 0, 1);
            }
        }, (1000*60/rythme) , (1000*60/rythme));

        bstart.setText(R.string.stop);
        play = true;
    }

    private void stop(){
        timer.cancel();
        bstart.setText(R.string.start);
        play = false;
    }

    @Override
    public void onResume(){
        super.onResume();
        if (play){stop();}
    }

    @Override
    public void onPause(){
        super.onPause();
        if (play){stop();}
    }
}