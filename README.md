Une documentation est disponible dans le dossier doc.

# Permissions
-  https://www.geeksforgeeks.org/android-how-to-request-permissions-in-android-application/
- https://developer.android.com/training/permissions/requesting

# Audio Processing
- https://medium.com/@juniorbump/pitch-detection-in-android-using-tarsosdsp-a2dd4a3f04e9
- https://0110.be/posts/TarsosDSP_on_Android_-_Audio_Processing_in_Java_on_Android

# Correspondance note-fréquence
- https://www.seventhstring.com/resources/notefrequencies.html
